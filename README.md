# c7-conda-lgarciac-cuda101

CentOS-7 based container (can be run on tars) without "kernel too old" issue
with miniconda installed and configured with CUDA-10.1 for gpu acceleration
(only the P100 nodes have the proper Nvidia driver version)

Tested with singularity 3.5.2

There is a prebuilt version at ~tru/singularity.d/containers/c7-conda-lgarciac-2020-04-15-1801.sif.

Building your own after adapting the code (on your own resources, you need to have "root" privileges):
```
sudo singularity build c7-conda-lgarciac-cuda101.sif Singularity
```

Transfer to tars (scp/sftp/rsync/...) and run with:
```
$ module add singularity/3.5.2
$ srun --qos gpu --gres=gpu:teslaP100:1 -p gpu -n1 -N1 --pty --preserve-env singularity exec -B /pasteur --nv ~tru/singularity.d/containers/c7-conda-lgarciac-2020-04-15-1801.sif  python3 ~tru/python/torch-cuda.py 
```

Adapt with the number of cores, gpus, ...

